//
// Created by pbialas on 25.09.2020.
// Worked on by Aneta Stajkowska
//

#include "app.h"

#include <iostream>
#include <vector>
#include <tuple>
#include <glm/matrix.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "Application/utils.h"

void SimpleShapeApplication::init() {


    auto program = xe::create_program(std::string(PROJECT_DIR) + "/shaders/base_vs.glsl",
                                      std::string(PROJECT_DIR) + "/shaders/base_fs.glsl");

    auto u_modifiers_index = glGetUniformBlockIndex(program, "Modifiers");
    if (u_modifiers_index == GL_INVALID_INDEX) { 
        std::cout << "Cannot find Modifiers uniform block in program" << std::endl; }
    else { 
        glUniformBlockBinding(program, u_modifiers_index, 1); }

    auto u_matrix_index = glGetUniformBlockIndex(program, "Matrices");
    if (u_matrix_index == GL_INVALID_INDEX) {
        std::cout << "Cannot find Matrices uniform block in program" << "\n";    }
    else {
        glUniformBlockBinding(program, u_matrix_index, 0);    }

    if (!program) {
        std::cerr << "Cannot create program from " << std::string(PROJECT_DIR) + "/shaders/base_vs.glsl" << " and ";
        std::cerr << std::string(PROJECT_DIR) + "/shaders/base_fs.glsl" << " shader files" << std::endl;
    }
   

    std::vector<GLfloat> vertices = {
       -0.5f, -0.5f, -0.5f, 0.0f, 1.0f, 0.0f,
       0.5f, -0.5f, -0.5f, 0.0f, 1.0f, 0.0f,
       0.5f, -0.5f, 0.5f, 0.0f, 1.0f, 0.0f,
       -0.5f, -0.5f, 0.5f, 0.0f, 1.0f, 0.0f,

       -0.5f, -0.5f, -0.5f, 0.0f, 1.0f, 0.0f,
       0.5f, -0.5f, -0.5f, 0.0f, 1.0f, 0.0f,
       0.0f, 0.5f, 0.0f,0.0f, 1.0f, 0.0f,

       0.5f, -0.5f, -0.5f, 0.0f, 0.0f, 1.0f,
       0.5f, -0.5f, 0.5f, 0.0f, 0.0f, 1.0f,
       0.0f, 0.5f, 0.0f, 0.0f, 0.0f, 1.0f,

       0.5f, -0.5f, 0.5f, 0.0f, 0.0f, 1.0f,
       -0.5f, -0.5f, 0.5f, 0.0f, 0.0f, 1.0f,
       0.0f, 0.5f, 0.0f, 0.0f, 0.0f, 1.0f,

       -0.5f, -0.5f, 0.5f, 0.0f, 0.0f, 1.0f,
       -0.5f, -0.5f, -0.5f, 0.0f, 0.0f, 1.0f,
       0.0f, 0.5f, 0.0f, 0.0f, 0.0f, 1.0f

    };


    std::vector<GLushort> indices = {
       0,1,2,2,
       3,0,4,
       5,6,7,
       8, 9, 10,
        11, 12, 13, 14
    };


    GLuint idx_buffer_handle;
    glGenBuffers(1, &idx_buffer_handle);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, idx_buffer_handle);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLushort), indices.data(), GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    GLuint v_buffer_handle;
    glGenBuffers(1, &v_buffer_handle);
    glBindBuffer(GL_ARRAY_BUFFER, v_buffer_handle);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(GLfloat), vertices.data(), GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 1);

    glGenVertexArrays(1, &vao_);
    glBindVertexArray(vao_);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, idx_buffer_handle);
    glBindBuffer(GL_ARRAY_BUFFER, v_buffer_handle);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), reinterpret_cast<GLvoid*>(0));
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), reinterpret_cast<GLvoid*>(3 * sizeof(GLfloat)));
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    float strength = 0.9f;
    float light[3] = { 0.5f, 0.5f, 0.5f };
    GLuint ubo_handle(0u);
    glGenBuffers(1, &ubo_handle);
    glBindBuffer(GL_UNIFORM_BUFFER, ubo_handle);
    glBufferData(GL_UNIFORM_BUFFER, 8 * sizeof(float), nullptr, GL_STATIC_DRAW);
    glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(float), &strength);
    glBufferSubData(GL_UNIFORM_BUFFER, 4 * sizeof(float), 3 * sizeof(float), light);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
    glBindBufferBase(GL_UNIFORM_BUFFER, 1, ubo_handle);
    glBindBuffer(GL_ARRAY_BUFFER, 1);

    int w, h;
    std::tie(w, h) = frame_buffer_size();
    glm::mat4 M(1.0f);
    glm::vec3 cameraPos = { 2.0f, 3.0f,  1.0f };
    glm::vec3 cameraCenter = { 0.0f, 0.0f, 0.0f };
    glm::vec3 cameraUp = { 0.0f, 1.0f, 0.0f };
    glm::mat4 V = glm::lookAt(cameraPos, cameraCenter, cameraUp);
    glm::mat4 P = glm::perspective(glm::pi<float>() / 3.0f, (float) w/h, 1.0f, 10.0f);
    glm::mat4 PMV = P * M * V;
    GLuint ubo_mat(0u);
    glGenBuffers(1, &ubo_mat);
    glBindBuffer(GL_UNIFORM_BUFFER, ubo_mat);
    glBufferData(GL_UNIFORM_BUFFER, sizeof(glm::mat4), nullptr, GL_STATIC_DRAW);
    glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(glm::mat4), &PMV[0]);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
    glBindBufferBase(GL_UNIFORM_BUFFER, 0, ubo_mat);

    glClearColor(0.81f, 0.81f, 0.8f, 1.0f);
    glViewport(0, 0, w, h);

    glEnable(GL_DEPTH_TEST);
    glUseProgram(program);
}

void SimpleShapeApplication::frame() {
    glBindVertexArray(vao_);
    glDrawElements(GL_TRIANGLES, 18, GL_UNSIGNED_SHORT, 0);
    glBindVertexArray(0);
}

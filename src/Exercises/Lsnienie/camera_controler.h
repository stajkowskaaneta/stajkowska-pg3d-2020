#include "camera.h"

class CameraControler {
public:
    CameraControler() :camera_(nullptr) {}
    CameraControler(Camera* camera) :camera_(camera) {}
    void set_camera(Camera* camera) { camera_ = camera; }
    void rotate_camera(float dx, float dy);
    void mouse_moved(float x, float y);
    void LMB_pressed(float x, float y);
    void LMB_released(float x, float y);

private:
    Camera* camera_;
    float scale_ = 0.1f;
    bool LMB_pressed_ = false;
    float x_ = 0;
    float y_ = 0;
};
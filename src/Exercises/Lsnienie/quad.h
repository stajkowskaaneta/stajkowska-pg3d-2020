#pragma once
#include <vector>
#include <memory>
#include "glad/glad.h"
#include <glm/glm.hpp>
#include "phong_material.h"

class Quad {
public:
    Quad();
    ~Quad() = default;
    Quad(const Quad& rhs) = delete;
    Quad& operator=(const Quad& rhs) = delete;
    std::shared_ptr<Quad> quad_;
    void operator=(Quad && rhs) = delete;
    Quad(Quad && rhs) = delete;
    void draw();

    PhongMaterial *material_;
    void set_material(PhongMaterial* new_material);
    PhongMaterial* get_material();


private:
    GLuint vao_;
    GLuint buffer_[2];
    //GLuint diffuse_texture_;
   // GLuint shininess_texture_;

    std::vector<GLushort> indices = {
            0, 1, 2, 0, 2, 3, 
    };

    std::vector<GLfloat> vertices = {
            -1.0f, 0.0f, -1.0f, 0.0, 0.0, 0.0, 1.0, 0.0,
            -1.0f, 0.0f, 1.0f, 0.0, 1.0, 0.0, 1.0, 0.0,
            1.0f, 0.0f, 1.0f, 1.0, 1.0, 0.0, 1.0, 0.0,
            1.0f, 0.0f, -1.0f, 1.0, 0.0, 0.0, 1.0, 0.0
    };
};
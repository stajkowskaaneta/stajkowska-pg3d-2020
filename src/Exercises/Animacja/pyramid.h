#pragma once
#include <memory>
#include <vector>
#include "glad/glad.h"


class Pyramid {
public:
    Pyramid();
    ~Pyramid() = default;
    Pyramid(const Pyramid& rhs) = delete;
    Pyramid& operator=(const Pyramid& rhs) = delete;

    void operator=(Pyramid&& rhs) = delete;
    Pyramid(Pyramid&& rhs) = delete;
    void draw();
private:
    GLuint vao_;
    GLuint buffer_[2];
    std::shared_ptr<Pyramid> pyramid_;

    std::vector<GLfloat> vertices = {
        -0.5f, 0.0f, 0.5f, 0.0f, 1.0f, 0.0f,
         0.5f, 0.0f, -0.5f, 0.0f, 1.0f, 0.0f,  //1 t
        -0.5f, 0.0f, -0.5f, 0.0f, 1.0f, 0.0f,

        -0.5f, 0.0f, 0.5f, 0.0f, 1.0f, 0.0f,
         0.5f, 0.0f, 0.5f, 0.0f, 1.0f, 0.0f,   //2 t
         0.5f, 0.0f, -0.5f, 0.0f, 1.0f, 0.0f,

         -0.5f, 0.0f, 0.5f, 0.0f, 0.0f, 1.0f,
         0.0f, 0.5f, 0.0f, 0.0f, 0.0f, 1.0f,   //3 t - niebieski
         0.5f, 0.0f, 0.5f, 0.0f, 0.0f, 1.0f,

         0.5f, 0.0f, 0.5f, 1.0f, 0.0f, 0.0f,
         0.0f, 0.5f, 0.0f, 1.0f, 0.0f, 0.0f,   //4 t
         0.5f, 0.0f, -0.5f, 1.0f, 0.0f, 0.0f,

        -0.5f, 0.0f, -0.5f, 1.0f, 1.0f, 0.0f,
         0.5f, 0.0f, -0.5f, 1.0f, 1.0f, 0.0f,  //5 t
         0.0f, 0.5f, 0.0f, 1.0f, 1.0f, 0.0f,

         0.0f, 0.5f, 0.0f, 0.0f, 1.0f, 1.0f,
        -0.5f, 0.0f, 0.5f, 0.0f, 1.0f, 1.0f,   //6 t
        -0.5f, 0.0f, -0.5f, 0.0f, 1.0f, 1.0f,

    };


    std::vector<GLushort> indices = {
        0,1,2,0,4,1,6,7,8,9,10,11,12,13,14,15,16,17

        //CW
        //17,16,15,14,13,12,11,10,9,8,7,6,1,4,0,2,1,0
    };
};
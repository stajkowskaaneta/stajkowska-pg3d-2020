#version 410

layout(location=0) in vec4 a_vertex_position;
layout(location=1) in vec4 a_vertex_color;
out vec4 vertex_color;

layout(std140) uniform Matrices{
    mat4 PMV;
    };

void main() {
    gl_Position = PMV*a_vertex_position;
    vertex_color = a_vertex_color;

}
